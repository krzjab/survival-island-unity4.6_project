﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionsV1 : MonoBehaviour { //ten scrypt C# umieszczamy na kontrolerze postaci , tagi odnosnie drzwi ktore podlegaja animacji)
	bool doorIsOpen = false;
	float doorTimer = 0.0f;
	public float doorOpenTime = 3.0f;
	public AudioClip doorOpenSound;
	public AudioClip doorShutSound;
	GameObject currentDoor;
	//kolejna zmienna bedzie uzyta przy Shutdoor()



	// Use this for initialization
	void Start () {
	
	}
	// Update is called once per frame
	void Update(){
		if(doorIsOpen){
			doorTimer += Time.deltaTime;
			if(doorTimer>doorOpenTime){
				// ShutDoor(currentDoor); //funkcja zamykania drzwi 
				Door(doorShutSound, false, "doorshut", currentDoor);
				doorTimer=0.0f; //wyzerowanie zmiennej czasowej
				
			}
		}

	} 
	

	void OnControllerColliderHit(ControllerColliderHit hit){
		if(hit.gameObject.tag == "playerDoor" && doorIsOpen == false){
			currentDoor = hit.gameObject;
		//	OpenDoor(currentDoor); // wlasna funkcja ktora odwoluje sie do argumentu "hit" 

			Door(doorOpenSound, true, "dooropen", currentDoor);
		}

	}


	/* void OpenDoor(GameObject doorTimer){
		doorIsOpen = true;
		GameObject.Find("door").audio.PlayOneShot(doorOpenSound); // obiekt drzwi, odwolanie do componentu audio, jednorazowe odtworzenie pliku, podanie pliku ktory jest "doorOpenSound" czyli dowolna zmienna podana w panelu inspector
		GameObject.Find("door").transform.parent.animation.Play ("dooropen");
		
	} */ //wylaczenie starej funkcji, zastepuje ją funkcja uniwersalną na 4 argumenty


/*	void ShutDoor(GameObject door){
		doorIsOpen = false;
		GameObject.Find("door").audio.PlayOneShot(doorShutSound);
		GameObject.Find("door").transform.parent.animation.Play("doorshut");
	} */
	void Door(AudioClip aClip,bool openCheck, string animName, GameObject thisDoor){ // argument "thisDoor" jako string czy jako GameObject?? uwzglednij i popraw jakby cos
		/*GameObject.Find(thisDoor).audio.PlayOneShot(aClip);

		GameObject.Find(thisDoor).transform.parent.animation.Play (animName);
		*/
		doorIsOpen = openCheck;
		thisDoor.audio.PlayOneShot(aClip);
		thisDoor.transform.parent.animation.Play(animName);
	}
	                              
}
