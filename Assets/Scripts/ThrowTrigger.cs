﻿using UnityEngine;
using System.Collections;

public class ThrowTrigger : MonoBehaviour {

	public AudioClip wejdzNaPlyte;
	public GUITexture crosshair;
	public GUIText textHints;


	void OnTriggerEnter(Collider col){
		if(col.gameObject.tag == "Player"){
			CoconutThrower.canThrow=true;
			audio.PlayOneShot(wejdzNaPlyte);
			crosshair.enabled=true;
			if(!CoconutWin.haveWon){
				textHints.SendMessage("ShowHint", "\n\n\n\n\n Do tej gry dodana jest bateria, \n może wygram baterię jeśli trafię wszystkie cele...");
			}
		}
	}

	void OnTriggerExit(Collider col){
		if(col.gameObject.tag == "Player"){
			CoconutThrower.canThrow=false;
			crosshair.enabled=false;
		}
	}

}
