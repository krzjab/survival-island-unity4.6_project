﻿using UnityEngine;
using System.Collections;

public class WinGame : MonoBehaviour {


	public GameObject winSequence; // strona 359
	public GUITexture fader;
	public AudioClip winClip;
	public GUITexture ekran;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator GameOver(){    // str 359
		AudioSource.PlayClipAtPoint(winClip, transform.position); /// str 360
		Instantiate(winSequence);
		// str 360
		yield return new WaitForSeconds(4.0f);
		Instantiate(fader);
		yield return new WaitForSeconds(2.0f);
		Instantiate(ekran);




	}

}
