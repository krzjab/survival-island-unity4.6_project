﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]

public class CoconutThrower : MonoBehaviour {

	public AudioClip throwSound;
	public Rigidbody coconutPrefab;
	public float throwSpeed = 30.0f;
	public static bool canThrow = false;// statyczna sprawia ze nie jest modyfikowalna w panelu inspector, ale nadal ma charakter globalny zmiennej publicznej
	//ta zmienna ma za zadnie byc wykorzystana w skrypcie ktory sprawdza czy gracz jest w strefie gdzie moze rzucac kokosami

	// Update is called once per frame
	void Update () { //funkcja update
		if(Input.GetButtonDown ("Fire1") && canThrow==true){ //instrukcja if
			audio.PlayOneShot (throwSound); // dzwiek rzucanego kokosa
			Rigidbody newCoconut = Instantiate(coconutPrefab, transform.position, transform.rotation) as Rigidbody; // wygenerowanie nowego obiektu
			newCoconut.name = "coconut"; // przyypisanie nazwy nowo utowrzonemu obiektowi
			newCoconut.velocity = transform.forward*throwSpeed;
			Physics.IgnoreCollision (transform.root.collider, newCoconut.collider, true);

		}

	
	}
}
